# Key Rack

[![Get it on Flathub](https://flathub.org/api/badge?locale=en)](https://flathub.org/apps/app.drey.KeyRack)

Key Rack allows to view and edit keys, like passwords or tokens, stored by apps. It supports Flatpak secrets as well as system wide secrets.

![Key Rack](https://gitlab.gnome.org/-/project/23102/uploads/f37e08127a1336d74e255a7b959b0185/image.png)
